﻿app.factory('socket', function ($rootScope, $localStorage, $interval) {

  var serverPath = $rootScope.serverPath;
  var socket = null;
  var delivery = null;
  var callBacks = {};

  var socketHandlers = {};


  var Connect = function () {
    if ($rootScope.BackendOnline == false || socket != null || $localStorage.NixERPData == null) {
      return;
    }

    if (typeof(io) == "undefined") {
      return;
    }

    socket = io.connect($rootScope.serverPath, {
      'query': 'token=' + $localStorage.NixERPData.Token
    });

    socket.on('connect', function (err, data) {
      $rootScope.BackendOnline = true;

      delivery = new Delivery(socket);

      delivery.on('send.success', function (fileUID) {
        if (angular.isDefined(fileUID)) {
          callBacks[fileUID.uid].callBack(true, callBacks[fileUID.uid].url);
          delete callBacks[fileUID.uid];
        }
      });


      delivery.on('send.error', function (fileUID) {
        if (angular.isDefined(fileUID)) {
          callBacks[fileUID.uid].callBack(false, callBacks[fileUID.uid].url);
          delete callBacks[fileUID.uid];
        }
      });

      for (handler in socketHandlers) {
        AddHandler(handler, socketHandlers[handler]);
      }
    });

    socket.on("error", function (error) {
      if (error.type == "UnauthorizedError" || error.code == "invalid_token") {
        // redirect user to login page perhaps?
        console.log("User's token has expired");
        $localStorage.NixERPData = null;
        $rootScope.User = null;
        $rootScope.$apply();
      }
    });

    socket.on('userobject', function (userObj) {
      $localStorage.NixERPData.User = userObj;
      $rootScope.User = userObj;
      $rootScope.$apply();
    });

    socket.on('disconnect', function (err) {
      console.log("Disconnected");
      if (socket != null) {
        $rootScope.BackendOnline = false;
      }
    });
  };

  var AddHandler = function (eventName, callback) {
    socket.on(eventName, function () {
      var args = arguments;
      $rootScope.$apply(function () {
        callback.apply(socket, args);
      });
    });
  }

  var Disconnect = function () {
    if (socket != null) {
      var tmpSocket = socket;
      socket = null;
      tmpSocket.disconnect();
    }
  }


  if (typeof(io) == "undefined") {
    var ioTimer = $interval(function () {
      if (typeof(io) != "undefined") {
        $interval.cancel(ioTimer);
        Connect();
      }
    }, 500);
  }
  else {
    Connect();
  }


  return {
    connect: Connect,
    disconnect: Disconnect,
    logout: function () {
      $localStorage.NixERPData = null;
      $rootScope.User = null;
    },
    on: function (eventName, callback) {
      if ($rootScope.Authenticated == false) {
        Connect();
      }

      socketHandlers[eventName] = callback;

      if ($rootScope.Authenticated) {
        AddHandler(eventName, callback);
      }
    },
    emit: function (eventName, data, callback) {
      if ($rootScope.Authenticated == false) {
        Connect();
      }

      socket.emit(eventName, data, function (name) {

        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    },
    sendFile: function (file, NixObjectType, ObjectID, callBack) {
      var a = delivery.send(file, {NixObjectType: NixObjectType, ObjectID: ObjectID});
      callBacks[a] = {
        url: "/" + NixObjectType + "/" + ObjectID + "/" + encodeURIComponent(file.name),
        callBack: callBack
      };
    },
    getSocketURL: function () {
      return "http://" + serverPath;
    }
  };
});
