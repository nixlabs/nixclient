app.factory('NixDmsServices', ['$rootScope', '$localStorage', 'RestifyHelper', '$q', 'Upload', function ($rootScope, $localStorage, RestifyHelper, $q, Upload) {
	return {
		UploadFile: function (File, NixObjectType, ObjectID,progressCallBack) {
			var deferred = $q.defer();
			var promise = deferred.promise;

			Upload.upload({
				url: $rootScope.serverUrl + '/dms/upload',
				data: {file: File, 'data': JSON.stringify({NixObjectType: NixObjectType, ObjectID: ObjectID})}
			}).then(function (resp) {
				deferred.resolve(resp.data);
			}, function (resp) {
				deferred.reject(resp.status);
			}, function (evt) {
				progressCallBack(100.0 * evt.loaded / evt.total)
			});

			return promise;
		},
		GetAllFiles: function(NixObjectType, ObjectID){
			var deferred = $q.defer();
			var promise = deferred.promise;
			RestifyHelper.get('/dms/objectfiles/' + NixObjectType + '/' + ObjectID).then(function (response) {
				var error = false;

				if (response.status == 200) {
					deferred.resolve(response);
				}
				else {
					deferred.reject(error);
				}
			});
			return promise;

		},
		DownloadFile: function (FileInfo) {
			var deferred = $q.defer();
			var promise = deferred.promise;


			RestifyHelper.get('/dms/download/' + FileInfo.ID, {responseType: 'arraybuffer'}).then(function (response) {
				var error = false;
				if (response.status == 200) {
					saveAs(new Blob([response.data],{type:"application/octet-stream"}), FileInfo.Filename);
					deferred.resolve(response);
				}
				else {
					deferred.reject(error);
				}
			});

			return promise;
		},
		DownloadFileCustom: function (FileInfo) {
			var deferred = $q.defer();
			var promise = deferred.promise;


			RestifyHelper.get('/dms/download/' + FileInfo.ID, {responseType: 'arraybuffer'}).then(function (response) {
				var error = false;
				if (response.status == 200) {
					deferred.resolve(response);
				}
				else {
					deferred.reject(error);
				}
			});

			return promise;
		},
		DeleteFile: function (FileID) {
			var deferred = $q.defer();
			var promise = deferred.promise;
			RestifyHelper.post('/dms/delete/' + FileID).then(function (response) {
				var error = false;
				if (response.status == 200) {
					deferred.resolve(response);
				}
				else {
					deferred.reject(error);
				}
			});
			return promise;
		},
		GetSingleFile: function (FileID) {
			var deferred = $q.defer();
			var promise = deferred.promise;
			RestifyHelper.post('/dms/fileinfo/' + FileID).then(function (response) {
				var error = false;
				if (response.status == 200) {
					deferred.resolve(response);
				}
				else {
					deferred.reject(error);
				}
			});
			return promise;
		}
	}
}]);