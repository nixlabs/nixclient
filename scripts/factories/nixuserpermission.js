"use strict";

NixDataModule.factory('NixUserPermission', ['$rootScope', '$q', 'socket', '$http', function ($rootScope, $q, socket, $http) {

    return {
        getAllModalsPermissions: function () {
            var deferred = $q.defer();
            var promise = deferred.promise;
            socket.emit("models_get",{}, function (error, result) {
                if (error)
                    deferred.reject(error);
                else
                    deferred.resolve(result);
            });

            return promise;


        },
        getAllServicesPermissions: function () {
            var deferred = $q.defer();
            var promise = deferred.promise;
            socket.emit("services_get",{}, function (error, result) {
                if (error)
                    deferred.reject(error);
                else
                    deferred.resolve(result);
            });

            return promise;
        },

        updateServicePermissions: function (newData) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            socket.emit("services_update",newData, function (error, result) {
                if (error)
                    deferred.reject(error);
                else
                    deferred.resolve(result);
            });

            return promise;
        },
        updateModelsPermissions: function (newData) {
            var deferred = $q.defer();
            var promise = deferred.promise;
            socket.emit("models_update",newData, function (error, result) {
                if (error)
                    deferred.reject(error);
                else
                    deferred.resolve(result);
            });

            return promise;
        }


    }
}]);