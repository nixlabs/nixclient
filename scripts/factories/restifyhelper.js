app.factory('RestifyHelper', ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {

    var postRequest = function(path, data){
        return $http({
            method: 'POST',
            url: $rootScope.serverUrl + path,
            withCredentials: true,
            data: data
        });
    };

    var getRequest = function(path, options){

        var conf = {
            method: 'GET',
            url: $rootScope.serverUrl + path,
            withCredentials: true
        };

        if(options){
            Object.assign(conf, options);
        }

        return $http(conf);
    };

    return {
        post: postRequest,
        get: getRequest,
        post_nixdata: function(dataLoadContextes){
            var tmpRefLoadContextes = [];

            if(Array.isArray(dataLoadContextes) == false){
                dataLoadContextes = [dataLoadContextes];
            }

            for (var i = 0; i < dataLoadContextes.length; i++) {
                tmpRefLoadContextes.push(dataLoadContextes[i].getRefDataContext());
            }

            var deferred = $q.defer();
            var promise = deferred.promise;
            
            $rootScope.currentPromise = promise;


            postRequest('/nixdata', tmpRefLoadContextes).then(function (response) {
                var result = response.data;
                var error = false;

                var res = [];

                for (var i = 0; i < result.length; i++) {
                    var tmp = result[i];
                    var tmpDLC = dataLoadContextes.NixFilter("ID",tmp.ID);
                    if (tmp.success) {
                        tmpDLC.setData(tmp.data);

                        if(tmp.TotalItems){
                            tmpDLC.setTotalItems(tmp.TotalItems);
                        }

                        tmpDLC.State = Nix.Enum.DataContextStates.Completed;
                        res.push(tmpDLC.Data);
                    }
                    else {
                        tmpDLC.error = tmp.error;
                        tmpDLC.State = Nix.Enum.DataContextStates.Error;
                        error = tmp.error;
                    }
                };

                if(res.length == 1){
                    res = res[0];
                }

                if (error)
                    deferred.reject(error);
                else
                    deferred.resolve(res);
            });

            return promise;
        },
        post_nixdatasubmit: function(nixObjects){
            var tmpRefObjects = [];

            var objNames = "";

            if(Array.isArray(nixObjects) == false){
                nixObjects = [nixObjects];
            }

            for (var i = 0; i < nixObjects.length; i++) {
                tmpRefObjects.push(nixObjects[i].GetObjectForDAL());

                if( i > 0){
                    objNames += ', ';
                }

                objNames += nixObjects[i].GetNixDataObject().TableName;

            }

            var deferred = $q.defer();
            var promise = deferred.promise;
            $rootScope.currentPromise = promise;

            postRequest('/nixdatasubmit', tmpRefObjects).then(function (response) {
                var results = response.data;
                var error = false;

                for (var i = 0; i < results.length; i++) {
                    if(results[i].Success == false){
                        error = results[i].Error;
                        console.log(results[i].Error);
                        if(results[i].Error.message){
                            error = nixObjects[i].GetNixDataObject().TableName + ", error:" + results[i].Error.message;
                        }

                    }
                    else {
                        nixObjects[i].UpdateObjectState(results[i]);
                    }
                }

                if (error){
                    deferred.reject(error);
                    $rootScope.showActionToast(error);
                }

                else{
                    deferred.resolve(results);
                    $rootScope.showActionToast("Saved " + objNames);
                }
            });

            return promise;
        },
        post_activatemembership: function (UserID) {


            var deferred = $q.defer();
            var promise = deferred.promise;

            postRequest('/user/activatemembership', UserID).then(function (response) {
                var results = response.data;
                var error = false;

                if(results.Success == false) {
                    error = results[i].Error;
                    console.log(results[i].Error);
                }

                if (error){
                    deferred.reject(error);
                    $rootScope.showActionToast(error);
                }

                else{
                    deferred.resolve(results);
                    $rootScope.showActionToast("Membership Activated");
                }
            });
            return promise;
        },
        get_logs:function(objectType, objectID){
            var deferred = $q.defer();
            var promise = deferred.promise;

            getRequest('/logs/' + objectType + '/' + objectID).then(function (response) {
                var results = response.data;
                var error = false;

                if (error){
                    deferred.reject(error);
                    $rootScope.showActionToast(error);
                }
                else{
                    deferred.resolve(results);
                }
            });
            return promise;
        }
    };
}]);