﻿"use strict";

NixDataModule.factory('NixDataProvider',['$rootScope', '$q', 'socket', 'RestifyHelper', 'NixDataContext','NixDataObject',
    function ($rootScope, $q, socket, RestifyHelper, NixDataContext,NixDataObject ){
    var DataLoadContexes = [];
    var DataLoadContexesDic = {};

    var DataObjects = [];

    return {
        //DataContext factory
        getDataContextForSingleObject: function (nixDataObject, ObjectID){
            var tmpNixObj = new NixDataContext(nixDataObject);
            tmpNixObj.addFilter("ID", ObjectID);

            return tmpNixObj;
        },
        getDataContext: function (nixDataObject) {
            return new NixDataContext(nixDataObject);
        },
        //Bulk Options
        addDataLoadContext: function (dataLoadContext){
            DataLoadContexes.push(dataLoadContext);
            DataLoadContexesDic[dataLoadContext.ID] = dataLoadContext;
        },
        removeDataLoadContext: function (dataLoadContext){

        },
        addNixObject: function(){
          //todo: add object to collection
        },
        removeNixObject: function(){
            //todo: remove object from collection
        },
        clearAll: function (){
            DataLoadContexes = [];
            DataObjects = [];
            DataLoadContexesDic = {};
        },
        loadAll: function (){
            var tmpDLCs = [];

            for (var i = 0; i < DataLoadContexes.length; i++) {
                if (DataLoadContexes[i].State == Nix.Enum.DataContextStates.New) {
                    DataLoadContexes[i].State = Nix.Enum.DataContextStates.Requested;
                    tmpDLCs.push(DataLoadContexes[i]);
                }
            };

            var deferred = $q.defer();
            var promise = deferred.promise;


            RestifyHelper.post('/nixdata', tmpDLCs).then(function (objectList) {
                var error = false;

                var res = [];

                for (var i = 0; i < objectList.length; i++) {
                    var tmp = objectList[i];
                    if (tmp.success) {
                        DataLoadContexesDic[tmp.ID].setData(tmp.data);
                        DataLoadContexesDic[tmp.ID].State = Nix.DataContextStates.Completed;
                        res.push(DataLoadContexesDic[tmp.ID].Data);
                    }
                    else {
                        DataLoadContexesDic[tmp.ID].error = tmp.error;
                        error = tmp.error;
                        DataLoadContexesDic[tmp.ID].State = Nix.DataContextStates.Error;
                    }
                };

                if (error)
                    deferred.reject(error);
                else
                    deferred.resolve(res);
            }, function (err) {
                deferred.reject(error);
            });

            return promise;
        },
        saveAll: function(){

        },
        //Data Functions
        loadNixObjects: RestifyHelper.post_nixdata,
        submitNixObjects: RestifyHelper.post_nixdatasubmit,
        //Short methods
        getAllNixObjects: function(nixDataObject){
            var dataContext = this.getDataContext(nixDataObject);
            return this.loadNixObjects(dataContext);
        },
        getSingleNixObject: function (nixDataObject, ID) {
            var dataContext = this.getDataContext(nixDataObject);
            dataContext.addFilter(nixDataObject.Fields.ID.name, ID);
            return this.loadNixObjects(dataContext);
        },



    }
}]);
