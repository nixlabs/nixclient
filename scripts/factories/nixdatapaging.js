(function () {
    'use strict';
    NixDataModule.factory("Paging", ['$http', '$rootScope', function ($http, $rootScope) {
        var StartPage;
        var StartLimit;
        function Paging(page,limit) {
            StartPage = page-1;
            StartLimit = limit;
        }
        Paging.prototype.OnPaginate = function (data,page,limit) {
            page-=1;
            var tmpData = [];
            angular.copy(data,tmpData);
            if (StartPage <= page) {
                var startp = limit * page;
                StartPage = page;
                return tmpData.splice(startp,limit);
            }
            else if (StartPage > page){
                var endp = (limit*page) + limit;
                var startp = endp-limit;
                StartPage = page;
                return tmpData.splice(startp,limit);
            }
            else if (StartLimit>limit){
                var startl = StartLimit * page;
                var endl = limit * page;
                StartLimit = limit;
                return tmpData.splice(startl,endl);
            }
            else if (limit>StartLimit){
                var startl = StartLimit * page;
                StartLimit = limit;
                return tmpData.splice(startl,limit);
            }
        };
        return Paging;
    }]);
}());
