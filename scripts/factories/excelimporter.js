"use strict";

NixDataModule.factory('ExcelImporter', ['$rootScope', '$http', '$q', function ($rootScope, $http, $q) {

    return {
        importFile: function (file) {
            var reader = new FileReader();
            reader.onload = function (e) {
                var data = e.target.result;
                var workbook = XLSX.read(data, {type: 'binary', sheetStubs: false});
                deferred.resolve(workbook);

            };

            reader.readAsBinaryString(file);

            var deferred = $q.defer();
            var promise = deferred.promise;

            return promise;
        },
        Nlb_BankReportParser: function (worksheet) {
            console.log(worksheet);
            var Report = {};
            var ReportLines = [];

            if (worksheet["U6"] != undefined && worksheet["U6"].w == "VISA") {
                var rowCount = 61;
                while (worksheet["B" + rowCount] != undefined && worksheet["B" + rowCount].w != null && worksheet["B" + rowCount].w != "") {
                    var obj = {};

                    dateString = worksheet["B" + rowCount].w;
                    var char = dateString.split(".");
                    obj.Date = new Date();
                    obj.Date.setDate(char[0]);
                    obj.Date.setMonth(parseInt(char[1]) - 1);
                    obj.Date.setYear(char[2]);


                    if (worksheet["V" + rowCount]) {
                        obj.Outcome = worksheet["V" + rowCount].w;
                    }
                    if (worksheet["AB" + rowCount]) {
                        obj.Income = worksheet["AB" + rowCount].w;
                    }
                    // if (worksheet["Z" + rowCount]) {
                    //     obj.Code = worksheet["Z" + rowCount].w;
                    // }
                    if (worksheet["I" + rowCount]) {
                        obj.Note = worksheet["I" + rowCount].w;
                    }
                    obj.Numbers = [];
                    if (worksheet["I" + (rowCount)]) {
                        obj.CompanyName = worksheet["I" + (rowCount)].w;
                    }

                    ReportLines.push(obj);

                    rowCount++;
                }
                var dateString = worksheet["Z16"].w;
                var dateSplit = dateString.split("-");
                if (dateSplit[0] != null && dateSplit[0] != undefined) {
                    dateString = dateSplit[0];
                    var char = dateString.split(".");
                    Report.Date = new Date();
                    Report.Date.setDate(char[0]);
                    Report.Date.setMonth(parseInt(char[1]) - 1);
                    Report.Date.setYear(char[2]);
                }

                if (dateSplit[1] != null && dateSplit[1] != undefined) {
                    dateString = dateSplit[1];
                    var char = dateString.split(".");
                    Report.DueDate = new Date();
                    Report.DueDate.setDate(char[0]);
                    Report.DueDate.setMonth(parseInt(char[1]) - 1);
                    Report.DueDate.setYear(char[2]);
                }
                Report.BankReportNumber = worksheet["S12"].w + worksheet["Y12"].w;
                Report.BankReportLines = ReportLines;
            }
            else {
                var rowCount = 31;
                var ZadCount = 31;
                while (!isNaN(worksheet["B" + rowCount].w)) {
                    var obj = {};
                    obj.RedenBroj = worksheet["B" + rowCount].w;
                    if (worksheet["N" + rowCount]) {
                        obj.Outcome = worksheet["N" + rowCount].w;
                    }
                    if (worksheet["S" + rowCount]) {
                        obj.Income = worksheet["S" + rowCount].w;
                    }
                    if (worksheet["Z" + rowCount]) {
                        obj.Code = worksheet["Z" + rowCount].w;
                    }
                    if (worksheet["AA" + rowCount]) {
                        obj.Note = worksheet["AA" + rowCount].w;
                    }
                    obj.Numbers = [];
                    if (worksheet["AK" + rowCount]) {
                        obj.Numbers.push(worksheet["AK" + rowCount].w);
                    }
                    ZadCount++;
                    if (worksheet["AK" + (rowCount + 2)]) {
                        obj.Numbers.push(worksheet["AK" + (rowCount + 2)].w);
                    }
                    if (worksheet["F" + (rowCount)]) {
                        obj.CompanyName = worksheet["F" + (rowCount)].w;
                    }

                    ReportLines.push(obj);

                    rowCount++;
                    while (worksheet["B" + rowCount] == undefined) {
                        rowCount++;
                    }
                }
                ;
                var dateString = worksheet["E10"].w;
                var char = dateString.split(" ");
                dateString = char[char.length - 1];
                char = dateString.split(".");
                var date = new Date();
                date.setDate(char[0]);
                date.setMonth(parseInt(char[1]) - 1);
                date.setYear(char[2]);
                Report.Date = date;
                Report.DueDate = null;
                Report.BankReportNumber = worksheet["P8"].w;
                Report.BankReportLines = ReportLines;

            }

            return Report;
        }


    };
}]);