﻿"use strict";

NixDataModule.factory('NixDataCache', ['$rootScope', '$q', 'socket', 'NixDataObject',
    function ($rootScope, $q, socket, NixDataObject) {

        var cachedObjects = {};
        socket.on("cachedata", function (cachedData) {
            console.log(cachedData);

            var objectKeys = Object.keys(cachedData);
            for (var i = 0; i < objectKeys.length; i++) {
                var nixDataObject = cachedData[objectKeys[i]];

                nixDataObject.NixData = [];
                // if(nixDataObject.Data != undefined)
                for (var j = 0; j < nixDataObject.Data.length; j++) {
                    nixDataObject.NixData.push(new NixDataObject(Nix.ObjectTypes[objectKeys[i]],null,nixDataObject.Data[j]));
                }
            }

            angular.merge(cachedObjects, cachedData);
            $rootScope.cacheInit = true;
        });

        return {
            getCachedObject: function (nixObjectType) {
                if(cachedObjects[nixObjectType])
                {
                    return cachedObjects[nixObjectType].NixData;
                }

                return null;
            }
        }

    }]);
