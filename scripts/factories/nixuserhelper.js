"use strict";

NixDataModule.factory('NixUserHelper', ['$rootScope', '$q', 'socket', '$http', '$localStorage', function ($rootScope, $q, socket, $http, $localStorage) {

  return {
    Authenticate: function (userObj) {
      return $http({
        method: 'POST',
        url: $rootScope.serverUrl + '/authenticate',
        withCredentials: true,
        data: userObj
      });
    },
    Logout: function () {
      $localStorage.NixERPData = null;
    },
    getAllUsers: function (data) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      socket.emit("users_get", data, function (error, result) {
        if (error)
          deferred.reject(error);
        else
          deferred.resolve(result);
      });

      return promise;
    },
    createUser: function (user) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      socket.emit("users_create", user, function (mergeObject) {
        if (mergeObject) {
          $rootScope.showActionToast("User created");
          deferred.resolve(mergeObject);
        }
        else {
          deferred.reject(mergeObject);
        }
      });
      return promise;
    },
    deleteUser: function (userID){
      var deferred = $q.defer();
      var promise = deferred.promise;
      socket.emit("users_delete", userID, function (status) {
        if (status) {
          $rootScope.showActionToast("User deleted");
          deferred.resolve(status);
        }
        else {
          $rootScope.showActionToast("Error");
          deferred.reject(status);
        }
      });
      return promise;
    },
    updateUser: function (user) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      socket.emit("users_update", user, function (mergeObject) {
        if (mergeObject == null) {
          $rootScope.showActionToast("User updated");
          deferred.resolve(mergeObject);
        }
        else {
          deferred.reject(mergeObject);
        }
      });
      return promise;
    },


    getAllUserGroups: function (data) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      socket.emit("usergroups_get", data, function (error, result) {
        if (error)
          deferred.reject(error);
        else {
          deferred.resolve(result);
        }
      });
      return promise;
    },
    createUserGroup: function (usergroup) {
      var deferred = $q.defer();
      var promise = deferred.promise;
      socket.emit("usergroups_create", usergroup, function (error) {
        if (error)
          deferred.reject(error);
        else {
          deferred.resolve(error);
        }


      });
      return promise;
    },
    updateUserGroup: function () {

    }
  }
}]);
