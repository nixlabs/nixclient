"use strict";

NixDataModule.directive('nixform', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: function (elem, context) {

            if (context.isrow == "true") {
                return 'bower_components/nixclient/scripts/templates/nixgridrow.html';
            }

            //var tmp = scope.NixObject;
            return "bower_components/nixclient/scripts/templates/nixform.html";
        },
        scope: {
            NixConfig: '=nixconfig',
            NixObject: '=nixobject',
            IsRow: '=isrow',
        },
        link: function (scope, element, attrs) {
            scope.tmpDate = null;
            scope.dataFields = [];
            scope.Fields = [];
            scope.Days = [];
            scope.showTooltip = true;
            scope.$watchCollection('[NixConfig, NixObject]', function (Attrs) {
                var NixConfig = Attrs[0];
                var NixObject = Attrs[1];
                if (angular.isDefined(NixConfig)) {
                    var NixFields = NixConfig.NixFields;
                    if (angular.isObject(NixObject) == false && angular.isArray(scope.NixData) == false) {
                        scope.NixObject = NixConfig.NixData;
                    }

                    for (var i = 0; i < NixFields.length; i++) {
                        if (NixFields[i].dataType == "dateSelect") {
                            try {
                                scope.tmpDate = NixObject[NixFields[i].name].getDate();
                            }
                            catch (exc) {

                            }
                            for (var i = 0; i < 31; i++) {
                                scope.Days[i] = i + 1;
                            }
                        }
                    }
                }
            });
            //autoselect
            // if((scope.NixConfig != null && scope.NixConfig != undefined) && (scope.NixObject != null && scope.NixObject != undefined)){
            //    for(var i=0;i < scope.NixConfig.NixFields.length;i++){
            //        if(scope.NixConfig.NixFields[i].dataType == 'select'){
            //            if(scope.NixObject[scope.NixConfig.NixFields[i].name] == null && scope.NixConfig.NixFields[i].fieldOptions.length > 0){
            //                scope.NixObject[scope.NixConfig.NixFields[i].name] = scope.NixConfig.NixFields[i].fieldOptions[0].value;
            //            }
            //        }
            //    }
            // }
            scope.placeholder = '';

            scope.ItemChanged = function (item, field) {
                if (scope.NixConfig.ItemChanged) {
                    if(field){
                    scope.NixConfig.ItemChanged(this.ctrl.searchText, item, field);
                    }
                    else{
                        scope.NixConfig.ItemChanged(this.ctrl.searchText, item);
                    }
                }
            };

            scope.deleteObject = function () {
                scope.NixObject.DeleteObject();
            };

            scope.DateChanged = function (Object, key) {
                Object[key].setDate(scope.tmpDate);
            };
        }
    }
});
