"use strict";

var Nix = Nix || {};
Nix.Config = Nix.Config || {};
Nix.ConfigAlias = Nix.ConfigAlias || {};


NixDataModule.directive('nixgrid', ['$timeout', 'Paging', '$mdEditDialog', '$mdDialog', function ($timeout, Paging, $mdEditDialog, $mdDialog) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'bower_components/nixclient/scripts/templates/nixgrid.html',
    scope: {
      NixConfig: '=nixconfig',
      NixObjects: '=nixobjects'
    },
    link: function (scope, element, attrs) {
      scope.gridConfig = new Nix.Config.NixFormConfig();
      scope.index = null;
      scope.GroupHeaders = {};
      scope.Counter = 0;
      scope.NixData = [];


      scope.ClickEnableDate = function () {
        scope.gridConfig.DisableDate(scope.gridConfig.testDate);
      };

      scope.ReferentItem = null;

      scope.ResetGrouping = function () {
        scope.GroupHeaders = {};
        scope.Counter = 0;
      };

      function MdAutocompleteCtrl($scope, $mdEditDialog) {
        console.log("im in dialog!");
        $scope.value = $scope.NixObject[$scope.item.name];
        $scope.ItemChanged = function (item) {
          if (scope.NixConfig.ItemChanged) {
            scope.NixConfig.ItemChanged(this.ctrl.searchText, item);
          }
        };

        $scope.dismiss = function () {
          $scope.NixObject[$scope.item.name] = $scope.value;
          // this.cancel();
        };
        $scope.submit = function () {
          console.log("dialog saved");
          //$mdEditDialog.cancel();
        };
      };

      scope.getRowClass = function (item) {
        if (scope.NixConfig.rowClass != undefined) {
          return scope.NixConfig.rowClass(item);
        }
        return false;
      }

      scope.editComment = function (event, item, object, dataType, placeholder) {
        event.stopPropagation(); // in case autoselect is enabled
        var inputType = 'text';
        if (dataType != 'select') {
          if (dataType == 'int' || dataType == 'decimal') inputType = 'number';

          if (dataType == 'string' && item.AdditionalData.length != 0) {

            var editDialog = {
              controller: MdAutocompleteCtrl,
              controllerAs: 'ctrl',
              //bindToController: true,
              targetEvent: event,
              focusOnOpen: true,
              scope: {NixObject: object, item: item},
              templateUrl: 'nixerp_client/scripts/templates/mdautocomplete.html',
              resolve: {
                // commenting out the row below will make the dialog appear again
              },
              //template: "<md-edit-dialog class=\"md-whiteframe-1dp\"><md-autocomplete md-autofocus md-selected-item=\"NixObject['NixAutoComplete']\" md-search-text-change=\"ItemChanged(NixObject)\" md-search-text=\"ctrl.searchText\" md-selected-item-change=\"ItemChanged(NixObject)\" md-items=\"obj in item.AdditionalData\" md-item-text=\"obj.Name\" md-min-length=\"0\" placeholder=\"Name\" ng-if=\"item.AdditionalData.length != 0 && item.dataType=='string'\" ng-class=\"!NixObject.IsValid(item.name) && NixConfig.Validation ? 'has-error' : ''\">  <md-item-template>  <span md-highlight-text=\"ctrl.searchText\" md-highlight-flags=\"^i\">{{obj.Name}}</span>  </md-item-template>  <md-not-found> No states matching \"{{ctrl.searchText}}\" were found.  <a ng-click=\"\">Create a new one!</a>  </md-not-found>  </md-autocomplete><md-edit-dialog>"

            };

            var promise;

            promise = $mdEditDialog.show(editDialog);

            promise.then(function (ctrl) {
              console.log(ctrl);
              //var input = ctrl.getInput();
              //input.$viewChangeListeners.push(function () {
              //    input.$setValidity('test', input.$modelValue !== 'test');
              //});
            });

          }
          else {
            var editDialog = {
              modelValue: object[item.name],
              placeholder: placeholder,
              save: function (input) {
                console.log(item);
                object[item.name] = input.$modelValue;
                //item = input.$modelValue;
                //scope.test = input.$modelValue;
                console.log(item);

              },
              targetEvent: event,
              title: placeholder,
              //validators: {
              //    'md-maxlength': 30
              //},
              type: inputType
            };

            var promise;

            promise = $mdEditDialog.large(editDialog);

            promise.then(function (ctrl) {
              var input = ctrl.getInput();
              //input.$viewChangeListeners.push(function () {
              //    input.$setValidity('test', input.$modelValue !== 'test');
              //});
            });
          }
        }
      };

      scope.addClass = function (item, plusItem) {
        return item + plusItem;
      }

      scope.$watchCollection('NixConfig', function (NixConfig) {
        if (NixConfig != undefined) {
          if (NixConfig.DataContext) {
            scope.NixData = NixConfig.DataContext.Data;
            if (scope.NixConfig.DataContext.OrderBy) {
              if (scope.NixConfig.DataContext.OrderBy.direction == "asc") {
                scope.orderBy = "-" + scope.NixConfig.DataContext.OrderBy.field;
              }
              else {
                scope.orderBy = scope.NixConfig.DataContext.OrderBy.field;
              }
            }
            scope.$watchCollection('NixConfig.DataContext.Data', function (nixData) {
              scope.NixData = nixData;
            });
          }
          else {
            if (NixConfig.ShowPaging) {
              scope.paging = new Paging(NixConfig.Page, NixConfig.Size);

              if (NixConfig.NixData.length > 0) {
                scope.NixData = scope.paging.OnPaginate(scope.NixConfig.NixData, NixConfig.Page, NixConfig.Size);
              }
            }
            else {
              scope.NixData = NixConfig.NixData;
            }
          }
          if (scope.NixObjects) {
            scope.NixData = scope.NixObjects;
          }

          if (NixConfig.NixFields.length == 0) {
            if (scope.NixData.length > 0) {
              NixConfig.GenerateFields();
            }
          }
          if (NixConfig.GroupBy != null) {
            scope.CreateGroupHeaders(scope.NixData);
          }

          // angular.merge(scope.gridConfig, NixConfig);

        }
      });

      scope.getTotalItems = function () {
        if (scope.NixConfig.DataContext.Paging) {
          if (scope.NixConfig && scope.NixConfig.DataContext && scope.NixConfig.DataContext.Paging.totalItems) {
            return scope.NixConfig.DataContext.Paging.totalItems;
          }
          else {
            return scope.NixConfig.NixData.length;
          }
        }
        else {
          return scope.NixConfig.NixData.length;
        }

        return 0;
      };

      scope.deleteObject = function (NixObject) {
        NixObject.DeleteObject();
      };

      scope.ClearSelectedItems = function () {
        scope.NixConfig.SelectedItems = [];
        scope.ReferentItem = null;
      };

      scope.GetDirectivesConfig = function (item) {
        if (item != null) {
          if (item.Config) {
            return item.Config;
          }
          var tmpConfig = new Nix.Config.NixFormConfig();
          tmpConfig.ShowLabels = true;
          tmpConfig.ShowPlaceholders = true;
          tmpConfig.IsEditable = scope.NixConfig.IsEditable;
          tmpConfig.NixFields = [
            // new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.ProductGroup.Fields.ID, "#"),
            // new Nix.ConfigAlias.NixFieldConfig(Nix.DAL.ProductGroup.Fields.Type, "Type"),
          ];
          tmpConfig.DataContext = item.DataContext;
          tmpConfig.GenerateFields();
          tmpConfig.DataContext = null;
          tmpConfig.NixData = item.Data;
          item.Config = tmpConfig;
          return item.Config;
        }
      };

      scope.IsItemSelected = function (nixObject) {
        return scope.NixConfig.SelectedItems.indexOf(nixObject) > -1;
      };
      scope.RowClick = function (nixObject) {
        if (scope.NixConfig && scope.NixConfig.RowClicked) {
          scope.NixConfig.RowClicked(nixObject);
        }
        else if (!scope.NixConfig.IsEditable) {
          if (event.shiftKey && scope.ReferentItem != null) {
            scope.NixConfig.SelectedItems = [];

            var tmp1 = scope.NixData.indexOf(nixObject);
            var tmp2 = scope.NixData.indexOf(scope.ReferentItem);

            if (tmp1 > tmp2) {
              var tmpx = tmp1;
              tmp1 = tmp2;
              tmp2 = tmpx;
            }

            for (var i = tmp1; i <= tmp2; i++) {
              scope.NixConfig.SelectedItems.push(scope.NixData[i]);
            }
          }
          else {
            var tmpIndex = scope.NixConfig.SelectedItems.indexOf(nixObject);

            if (event.ctrlKey == false) {
              scope.NixConfig.SelectedItems = [];
            }

            if (tmpIndex == -1) {
              if (scope.NixConfig.SelectedItems.length == 0) {
                scope.ReferentItem = nixObject;
              }

              scope.NixConfig.SelectedItems.push(nixObject);
            }
            else if (scope.NixConfig.SelectedItems.length > 0) {
              scope.NixConfig.SelectedItems.splice(tmpIndex, 1);
            }
          }
        }
      };
      scope.ItemChanged = function (item) {
        if (scope.NixConfig.ItemChanged) {
          scope.NixConfig.ItemChanged(this.ctrl.searchText, item);
        }
      };
      scope.FilterFunction = function (item) {
        var res = undefined;
        if (scope.NixConfig.FilterFunction) {
          res = scope.NixConfig.FilterFunction(item);
          if (scope.NixConfig.GroupBy != undefined) {
            scope.CreateGroupHeaders(scope.NixConfig.NixData);
          }
        }
        else
          res = true;

        if (res != undefined) {
          return res;
        }
      };
      scope.LoadObjects = function () {

        if (scope.NixConfig.UpdateQuery) {
          if (scope.NixConfig.DataContext.OrderBy != null) {
            scope.NixConfig.UpdateQuery(scope.NixConfig.DataContext.Paging.page, scope.NixConfig.DataContext.Paging.size, scope.NixConfig.DataContext.OrderBy.field, scope.NixConfig.DataContext.OrderBy.direction);
          }
          else {
            scope.NixConfig.UpdateQuery(scope.NixConfig.DataContext.Paging.page, scope.NixConfig.DataContext.Paging.size);
          }
        }
        if (scope.NixConfig.LoadObjects != undefined) {
          scope.NixConfig.LoadObjects();
        }
        else {
          scope.promise = scope.NixConfig.DataContext.LoadData();
        }
      };

      scope.Pagination = function (page, limit) {
        if (scope.NixConfig.DataContext) {
          scope.NixConfig.DataContext.setPage(page - 1, limit);
          scope.LoadObjects();
        }
        //todo: fire change event
      };

      scope.ClearFilter = function () {
        var checkFilter = Object.keys(scope.NixConfig.DataContext.Filters);
        if (checkFilter.length > 0) {
          scope.NixConfig.DataContext.clearFilter();
          scope.NixConfig.SearchFilter = "";
          scope.NixConfig.DataContext.Paging.page = 0;
          scope.LoadObjects();
        }
        else {
          scope.NixConfig.SearchFilter = "";
        }
      };

      scope.SearchFilterEvent = function (event) {
        if (event.keyCode == 13) {
          //enter btn
          scope.NixConfig.Search();
        }
        else if (event.keyCode == 27) {
          //esc btn
          scope.ClearFilter();
        }
      };
      var flag = false;
      scope.logOrder = function (item) {
        var orderDirection = '';
        if (flag) {
          flag = !flag;
          orderDirection = 'asc';
        }
        else {
          flag = !flag;
          orderDirection = 'desc';

        }
        var pom = item.split("");
        if (pom[0] == '-') {
          item = "";
          for (var i = 1; i < pom.length; i++) item += pom[i];
        }
        scope.NixConfig.DataContext.SetOrderBy(item, orderDirection);
        scope.LoadObjects();
      };


      scope.CreateGroupHeaders = function (Items) {
        var tmpItems = Items;
        scope.ResetGrouping();
        scope.tmpList = [];
        for (var j = 0; j < tmpItems.length; j++) {
          var item = tmpItems[j];
          if (scope.NixConfig.FilterFunction(item)) {
            scope.tmpList.push(item);
          }
        }
        Items = scope.tmpList;
        for (var i = 1; i < Items.length; i++) {
          var curObj = Items[i];
          var prevObj = Items[i - 1];
          var curLabel = scope.getMonthName(curObj[scope.NixConfig.GroupBy]) + " " + curObj[scope.NixConfig.GroupBy].getFullYear();
          var prevLabel = scope.getMonthName(prevObj[scope.NixConfig.GroupBy]) + " " + prevObj[scope.NixConfig.GroupBy].getFullYear();

          if (scope.Counter == 0) {
            if (scope.GroupHeaders[scope.Counter] == undefined) {
              scope.GroupHeaders[scope.Counter] = prevLabel;
              scope.Counter++;
            }
          }
          if (curLabel == prevLabel) {
            scope.Counter++;
          }
          else {
            if (scope.GroupHeaders[scope.Counter] == undefined) {
              scope.GroupHeaders[scope.Counter] = curLabel;
              scope.Counter++;
            }
          }
        }
        Items = tmpItems;
      };

      scope.selectedIndex = function (item) {
        scope.index = item.$index;
      };
      scope.NewLine = function () {
        if (scope.NixConfig && scope.NixConfig.NewLine) {
          scope.NixConfig.NewLine();
        }
      };
      scope.newLineKeyDown = function (event) {
        if (event.keyCode != 13 && event.keyCode != 27) {
          var pom = element[0].getElementsByTagName('input');
          if (pom.length > 0) {
            pom[pom.length - scope.NixConfig.NixFields.length].focus();
          }
        }
      };

      scope.Enum = function (item, val) {
        // for (var i = 0; i < Object.keys(item.enum).length; i++) {
        //   var key = Object.keys(item.enum)[i];
        //   if (item.enum[key] == val) {
        //     return key.replace(/([A-Z])/g, ' $1').trim();
        //   }
        // }
        return item.enum[val];
      };
      scope.getMonthName = function (Date) {
        var m = ['January', 'February', 'March', 'April', 'May', 'June', 'July',
          'August', 'September', 'October', 'November', 'December'];
        //Date = scope.NixConfig.NixData[Date].DateFormat(scope.NixConfig.NixData[Date][item.name]);
        return m[Date.getMonth()];
      };
      scope.getDayName = function (Date) {
        var d = ['Monday', 'Tuesday', 'Wednesday',
          'Thursday', 'Friday', 'Saturday', 'Sunday'];
        return d[Date.getDay()];
      };
      scope.searchFocus = function () {
        scope.showTextFilter = true;
        if (!scope.NixConfig.DateFilter) {
          $timeout(function () {
            $("#searchfield").focus();
          }, 1);
        }
      };
      scope.CloseFilter = function () {
        scope.showTextFilter = false;
        scope.ClearFilter();
      };
    }
  }
}]);


Nix.Config.NixFormConfig = function () {
  this.NixFields = []; //fields/columns config
  this.NixData = []; // data list
  this.IsEditable = false; // should show editable controls
  this.EnableNewLine = true;
  this.Validation = false; // show required fields
  this.RowClicked = undefined; // event for row clicked
  this.NewLine = undefined; // event for new field
  this.ShowLabels = true;
  this.ShowPlaceholders = true;
  this.FilterFunction = undefined;
  this.ItemChanged = undefined;
  this.CustomCreated = undefined;
  this.Reseted = false;
  this.GroupBy = null;
  this.ShowPaging = false;
  this.TotalItems = null;
  this.CreateNew = undefined;
  this.SearchFilter = null;
  this.LoadObjects = undefined;
  this.Search = undefined;
  this.SearchOpen = false;
  this.DateFilter = false;
  this.StartDateLabel = "Start Date";
  this.EndDateLabel = "End Date";
  this.StartDate = null;
  this.EndDate = null;
  this.ShowSearchInput = true;
  this.SelectedItems = [];
  this.DataContext = null;
  this.GridTitle = "";
  this.UpdateQuery = undefined;
  this.DialogEdit = false;
  this.PagingSizeOptions = [15, 30, 50];
  this.DisableDate = null;
  this.testDate = true;
  this.DecimalPlaces = 2;
  this.Page = 1;
  //New
  this.Title = null;

  this.GenerateFields = function () {
    //todo: generate fields from nixdata or add nixdataobject
    var field = null;
    for (field in this.DataContext.NixDataObject.Fields) {
      this.NixFields.push(new Nix.ConfigAlias.NixFieldConfig(Nix.DAL[this.DataContext.NixDataObject.TableName].Fields[field], field));

    }

  };

  this.CheckNullableFields = function (data) {
    var tmpData = data;
    if (tmpData == undefined) {
      tmpData = this.NixData;
    }

    if (Array.isArray(tmpData) == false) {
      tmpData = [tmpData];
    }

    for (var i = 0; i < this.NixFields.length; i++) {
      var tmpField = this.NixFields[i];
      if (tmpField.nullable == false) {
        for (var k = 0; k < tmpData.length; k++) {
          var tmpObj = tmpData[k];
          if (tmpObj[tmpField.name] == null) {
            switch (tmpField.dataType) {
              case "select":
                if (tmpField.fieldOptions.length > 0) {
                  tmpObj[tmpField.name] = tmpField.fieldOptions[0].value;
                }
                break;
              case "int":
              case "decimal":
                tmpObj[tmpField.name] = 0;
                break;
              case "datetime":
                //tmpObj[tmpField.name] = new Date();
                break;
            }
          }
        }
      }
    }
  }.bind(this);
};

Nix.ConfigAlias.NixFieldConfig = function (NixFieldObject, label, link, placeholder, datatype) {
  this.name = NixFieldObject.name;
  this.label = label || this.name;
  this.placeholder = placeholder || this.label || this.name;
  this.readOnly = NixFieldObject.readOnly;
  this.nullable = NixFieldObject.nullable;
  this.link = null;
  this.enum = {};
  this.isGroup = false;
  //this.autocompleteSearchBy = null;
  //this.autocompleteFilterBy = null;

  if (datatype == undefined) {
    this.dataType = NixFieldObject.datatype;
    if (this.dataType == "string" && NixFieldObject.length > 600) {
      this.dataType = "text";
    }
  }
  else
    this.dataType = datatype;

  if (this.dataType == "link") {
    if (link != undefined) {
      this.link = link;
    }
    else {
      this.link = this.name;
    }
  }
  this.fieldOptions = [];
  this.AdditionalData = [];
  this.SelectedAdditionalData = null;

  this.addFieldOption = function (value, label) {
    this.fieldOptions.push({
      value: value,
      label: label
    });
  };

  this.addFieldLink = function (value) {
    this.link = value;
  }

  this.autoCompleteFilter = function () {
    var results = query ? self.states.filter(createFilterFor(query)) : self.states,
      deferred;
    if (self.simulateQuery) {
      deferred = $q.defer();
      $timeout(function () {
        deferred.resolve(results);
      }, Math.random() * 1000, false);
      return deferred.promise;
    } else {
      return results;
    }
  }

};

Nix.ConfigAlias.NixGroupField = function (GroupID, GroupName) {
  this.GroupID = GroupID;
  this.isGroup = true;
  this.GroupName = GroupName;
};
