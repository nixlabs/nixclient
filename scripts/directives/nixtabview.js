"use strict";

var Nix = Nix || {};
Nix.Config = Nix.Config || {};
Nix.ConfigAlias = Nix.ConfigAlias || {};


NixDataModule.directive('nixtabview',function (NixDataProvider) {
    return{
        restrict: 'E',
        replace: true,
        templateUrl : 'bower_components/nixclient/scripts/templates/nixtabview.html',
        scope: {
            NixConfig: '=nixconfig',
            NixObjects: '=nixobjects'
        },
        link : function(scope, element, attrs){
            scope.tabConfig = new Nix.Config.NixFormConfig();
            scope.NixData = [];
            scope.tabs = [];
            scope.selectedTab = 0;
            scope.tmpDate = null;
            scope.Days = [];
            scope.$watchCollection('NixConfig', function (NixConfig) {
                if (NixConfig != undefined) {
                    if (NixConfig.DataContext) {
                        scope.NixData = NixConfig.DataContext.Data;
                        scope.$watchCollection('NixConfig.DataContext.Data', function (nixData) {
                            scope.NixData = nixData;
                        });
                    }
                    else {
                            scope.NixData = NixConfig.NixData;
                    }

                    if (scope.NixObjects) {
                        scope.NixData = scope.NixObjects;
                    }
                    if (NixConfig.NixFields.length == 0) {
                        if (scope.NixData.length > 0) {
                            NixConfig.GenerateFields();
                        }
                    }
                }

            });

            scope.removeObject = function(NixObject){
                NixObject.DeleteObject();
                var promise = NixDataProvider.submitNixObjects(NixObject);
                promise.then(function(){

                },function(reson){
                    console.log("Error: "+reason);
                });
            };

            scope.cancelChanges = function(NixObject){
              NixObject.ResetObject();
            };

            scope.saveChanges = function(NixObject){
                var promise = NixDataProvider.submitNixObjects(NixObject);
                promise.then(function(){

                },function(reson){
                    console.log("Error: "+reason);
                });
            };
        }
    }
});
