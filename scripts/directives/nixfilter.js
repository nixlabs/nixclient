"use strict";

var Nix = Nix || {};
Nix.Config = Nix.Config || {};
Nix.ConfigAlias = Nix.ConfigAlias || {};


NixDataModule.directive('nixfilter', ['$timeout', 'Paging', '$mdEditDialog', '$mdDialog', '$urlMatcherFactory', function ($timeout, Paging, $mdEditDialog, $mdDialog, $urlMatcherFactory) {
  return {
    restrict: 'E',
    replace: true,
    templateUrl: 'bower_components/nixclient/scripts/templates/nixfilter.html',
    scope: {
      NixConfig: '=nixconfig'
    },
    link: function (scope, element, attrs) {
      scope.showTextFilter = false;

      scope.searchFocus = function () {
        scope.showTextFilter = true;
        if (!scope.NixConfig.DateFilter) {
          $timeout(function () {
            $("#searchfield").focus();
          }, 1);
        }
      };

      scope.clearFilters = function () {
        scope.NixConfig.DataContext.clearFilter();
        for (var i = 0; i < scope.NixConfig.HiddenFilters.length; i++) {
          scope.NixConfig.DataContext.addFilter(scope.NixConfig.HiddenFilters[i].name, scope.NixConfig.HiddenFilters[i].value);
        }
      };
      scope.CloseFilter = function () {
        scope.showTextFilter = false;
        scope.clearFilters();
        var promise = scope.NixConfig.DataContext.LoadData();
        promise.then(function (res) {
          console.log("Data Context filtered");
        }, function (error) {
          console.log(error)
        });
        if (scope.NixConfig.state != undefined) {
          scope.NixConfig.state.go(scope.NixConfig.state.current, {filterdata: ""}, {notify: true});
        }
      };

      scope.FilterFunction = function (item) {
        scope.SearchClick(item);
        //scope.addFilter(item);
        //var promise = scope.NixConfig.DataContext.LoadData();
        //promise.then(function (res) {
        //    console.log("Data Context filtered");
        //}, function (error) {
        //    console.log(error)
        //});
      };

      scope.addFilter = function (item) {
        var tmpData = "";
        tmpData = item.data;
        if (item.data != undefined) {
          if (item.dataType == 'select') {
            scope.NixConfig.DataContext.addFilter(item.name, item.data);
          }
          else if (item.dataType == 'slider') {
            scope.NixConfig.DataContext.addFilter(item.name, {Value: item.data, Operator: item.Operator});
          }
          else if (item.dataType == 'date' || item.dataType == 'datetime') {
            var startDate = new Date(item.data);
            startDate.setHours(0);
            startDate.setMinutes(0);
            startDate.setSeconds(0);
            startDate.setMilliseconds(0);
            var endDate = new Date(startDate);
            endDate.setDate(endDate.getDate() + 1);
            endDate.setSeconds(endDate.getSeconds() - 1);
            scope.NixConfig.DataContext.addFilter(item.name, {
              AND: [{
                Value: endDate,
                Operator: "<="
              }, {Value: startDate, Operator: ">="}]
            });
          }
          else {
            scope.NixConfig.DataContext.addFilter(item.name, {Value: item.data, Operator: item.Operator});
          }
        }
        return {name: item.name, data: tmpData};
      };

      scope.ClickEnableDate = function () {
        alert();
      };

      scope.SearchClick = function (item) {
        var tmpFilter = {};
        scope.clearFilters();
        for (var i = 0; i < scope.NixConfig.NixFields.length; i++) {
          var tmpData = null;
          var item = scope.NixConfig.NixFields[i];
          if (item.dataType == 'datetime') {
            if (item.DateDisable) {

            }
            else {
              if (item.data != null) {
                tmpData = scope.addFilter(item);
              }
            }
          }
          else {
            if (item.data == "" || item.data == []) {

            }
            else if (item.OwnFunction != null) {
              item.OwnFunction(item);
            }
            else {
              tmpData = scope.addFilter(item);
            }
          }
          if (tmpData != null) {
            tmpFilter[tmpData.name] = tmpData.data;
          }
        }
        var promise = scope.NixConfig.DataContext.LoadData();
        promise.then(function (res) {
          console.log("Data Context filtered");
        }, function (error) {
          console.log(error)
        });
        if (scope.NixConfig.state != undefined) {
          scope.NixConfig.state.go(scope.NixConfig.state.current, {filterdata: escape(JSON.stringify(tmpFilter))}, {notify: false});
        }
      };

      scope.checkAutoSearch = function (field) {
        if (field.AutoSearch == true){
          if (field.OwnFunction != undefined){
            field.OwnFunction(field);
          }
          scope.SearchClick();
        }
      };

      scope.SearchFilterEvent = function ($event, item) {
        if (event.keyCode == 13) {
          //enter btn
          scope.FilterFunction(item);
        }
        else if (event.keyCode == 27) {
          //esc btn
          //scope.ClearFilter();
          scope.CloseFilter();
        }
      };
    }
  }
}]);


Nix.Config.NixFilterConfig = function (title) {
  this.GridTitle = title;
  this.NixFields = [];
  this.OnClick = true;

  this.DataContext = null;
  this.HiddenFilters = [];
  this.showTitle = true;
  this.DateDisable = null;


  this.GenerateFields = function () {
    //todo: generate fields from nixdata or add nixdataobject
  };
};

Nix.ConfigAlias.NixFilterFieldConfig = function (NixFieldObject, label, link, placeholder, datatype) {
  this.name = NixFieldObject.name;
  this.label = label || this.name;
  this.placeholder = placeholder || this.label || this.name;
  this.readOnly = NixFieldObject.readOnly;
  this.nullable = NixFieldObject.nullable;
  this.link = null;
  this.AutoSearch = false;
  this.isGroup = false;
  this.fieldOptions = [];
  this.multi = true;
  this.DefaultOptions = [];
  this.canDisableDate = false;
  this.Operator = null;
  this.data = "";
  this.OwnFunction = null;

  //this.AdditionalData = [];


  if (datatype == undefined) {
    this.dataType = NixFieldObject.datatype;
    if (this.dataType == "string" && NixFieldObject.length > 600) {
      this.dataType = "text";
    }
    else if (this.dataType == 'select') {
      this.data = [];
    }
    else if (this.dataType == 'datetime') {
      this.data = new Date();
    }
  }
  else {
    this.dataType = datatype;
  }

  if (this.dataType == 'int' || this.daraType == 'decimal' || this.dataType == 'datetime') {
    this.Operator = '=';
  }
  else {
    this.Operator = 'Like N';
  }

  if (this.dataType == "link") {
    if (link != undefined) {
      this.link = link;
    }
    else {
      this.link = this.name;
    }
  }

  this.addFieldOption = function (value, label) {
    this.fieldOptions.push({
      value: value,
      name: label
    });
  };

  this.addFieldLink = function (value) {
    this.link = value;
  }

};

Nix.ConfigAlias.NixGroupField = function (GroupID, GroupName) {
  this.GroupID = GroupID;
  this.isGroup = true;
  this.GroupName = GroupName;
};
