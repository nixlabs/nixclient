"use strict";

NixDataModule.directive('nixupload', ['NixDmsServices', '$rootScope', function (NixDmsServices, $rootScope) {
	return {
		restrict: 'E',
		replace: true,
		templateUrl: "bower_components/nixclient/scripts/templates/nixupload.html",
		scope: {
			NixConfig: '=nixconfig',
		},
		link: function (scope, element, attrs) {
			var NixObjectType = scope.NixConfig.NixObjectType;
			var NixObjectID = scope.NixConfig.NixObjectID;
			scope.files = [];
			scope.FilesInfo = [];
			scope.Progress = null;
			var promise = NixDmsServices.GetAllFiles(NixObjectType, NixObjectID);
			promise.then(function (res) {
				scope.FilesInfo = res.data;
			}, function (err) {
				console.log(err);
			});

			scope.ProgressFun = function (item) {
				scope.Progress = item;
			};

			scope.uploadFiles = function (files) {
				if (scope.NixConfig.FileLimit != null) {
					if (scope.NixConfig.FileLimit <= scope.FilesInfo.length) {
						$rootScope.showActionToast("Limit exceeded please remove a file");
						return;
					}

					if (files.length > scope.NixConfig.FileLimit) {
						$rootScope.showActionToast("Limit exceeded");
						return;
					}
				}
				if (scope.NixConfig.AllowedFileExtensions.length > 0) {
					var allowedExtension = false;
					for (var i = 0; i < files.length; i++) {
						var file = files[i];
						var fileExtension = file.type;
						if(fileExtension == undefined || fileExtension == null || fileExtension.length == 0){
							var splitted = file.name.split('.');
							if(splitted.length > 0) {
								var exten = splitted[splitted.length - 1];
								fileExtension = exten;
							}
						}
						for (var j = 0; j < scope.NixConfig.AllowedFileExtensions.length; j++) {
							var extension = scope.NixConfig.AllowedFileExtensions[j];
							if (fileExtension.toLowerCase().indexOf(extension) != -1) {
								allowedExtension = true;
								break;
							}
						}
					}

					if (allowedExtension == false) {
						$rootScope.showActionToast("File " + fileExtension + " is not allowed");
						return;
					}
				}
				scope.Progress = null;
				if (files && files.length) {
					for (var i = 0; i < files.length; i++) {
						if (scope.NixConfig.ProcessFile) {
							scope.NixConfig.ProcessFile(null, Nix.AppEnum.FileProcessTypes.uploading);
						}
						var promise = NixDmsServices.UploadFile(files[i], NixObjectType, NixObjectID, scope.ProgressFun);
						promise.then(function (res) {
							scope.FilesInfo.push(res);
							scope.files = [];
							if (scope.NixConfig.ProcessFile) {
								scope.NixConfig.ProcessFile(res, Nix.AppEnum.FileProcessTypes.uploaded);
							}
						}, function (err) {
							console.log(err);
						})
					}
				}
			};

			scope.removeFile = function (FileID, index) {
				if (scope.NixConfig.ProcessFile) {
					scope.NixConfig.ProcessFile(null, Nix.AppEnum.FileProcessTypes.deleted);
				}
				var promise = NixDmsServices.DeleteFile(FileID);
				promise.then(function (res) {
					console.log("file removeid " + FileID);
					scope.FilesInfo.splice(index, 1);
				}, function (err) {
					console.log(err);
				})
			};

			scope.showDetails = function (FileInfo) {
				if (scope.NixConfig.FileClick != undefined){
					scope.NixConfig.FileClick();
				}
				else {
					var promise = NixDmsServices.DownloadFile(FileInfo);
					promise.then(function (res) {
						console.log(res);
					}, function (err) {
						console.log(err);
					})
				}
			}
		}
	}
}]);

Nix.Config.NixUploadConfig = function (NixObjectType, ObjectID) {
	this.SelectButton = true;
	this.Drop = false;
	this.NixObjectType = NixObjectType || null;
	this.ObjectID = ObjectID || null;
	this.ProcessFile = undefined;
	this.AllowedFileExtensions = [];
	this.FileLimit = null;
	this.FileClick = undefined;
};
