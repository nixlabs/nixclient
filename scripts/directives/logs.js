"use strict";

NixDataModule.directive('logs', function () {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: "nixerp_client/scripts/templates/logs-details.html",
        scope: {
            LogsData: '=logsdata',
        },
        link: function (scope, element, attrs) {

            scope.$watch('LogsData', function (logsData) {

            });


            scope.Dict = {};
            scope.toggleTopDown = function (log) {
                if (scope.Dict[log.ID] == undefined) {
                    scope.Dict[log.ID] = log;
                }
                else {
                    delete scope.Dict[log.ID];
                }
            }
            scope.checkStatus = function (log) {
                if (scope.Dict[log.ID] != undefined) {
                    return true;
                }
                else
                    return false;
            }
        }
    }
});