﻿"use strict";

NixDataModule.service("NixDataObject", ['uuid2', 'NixDataObjectReference_Array', 'NixDataObjectReference_Object', '$state', '$stateParams', '$window', '$rootScope', function (uuid2, NixDataObjectReference_Array, NixDataObjectReference_Object, $state, $stateParams, $window, $rootScope) {

    var IsNullOrEmpty = function (tmp) {
        if (tmp == null) {
            return true;
        }
        else if (tmp == "") {
            return true;
        }
        return false;

    };

    var NixDataObject = function (nixDataObject, nixDataContext, data) {
        angular.extend(this, nixDataObject.BlankObject);

        var _nixDataObject = nixDataObject;
        var customCreated = true;

        this.NixObjectID = uuid2.newguid();
        this.NixObjectState = Nix.Enum.NixDataObjectStates.New;
        this.NixObjectExpired = false;

        var _nixDataContext = nixDataContext || null;

        this.getObjectsDataContext = function () {
            return _nixDataContext;
        };

        if (data) {
            if (nixDataObject.State != Nix.Enum.NixDataObjectStates.New) {
                data = this.NormalizeData(data, nixDataObject.Fields);
            }

            var mainFields = Object.keys(data);
            for (var i = 0; i < mainFields.length; i++) {
                var tmpField = mainFields[i];
                this[tmpField] = data[tmpField];
            }

            // angular.merge(this, data);
            this.NixObjectState = Nix.Enum.NixDataObjectStates.Loaded;
            customCreated = false;
        }

        var OriginalData = null;

        this.CheckHasExpired = function () {
            if (this.NixObjectExpired) {
                throw "Object has expired";
            }
        };

        this.CheckValidation = function () {
            var nixObject = this.GetNixDataObject().Fields;
            var fields = Object.keys(this.GetNixDataObject().Fields);
            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                if (!nixObject[field].identity && !nixObject[field].nullable && (this[field] == null || this[field] === "")) {
                    $rootScope.showActionToast("Error: Field " + field + " is not nullable");
                    return false;
                }
            }
            return true;
        };

        this.GetOriginalData = function () {
            return OriginalData;
        };

        this.SetOriginalData = function () {
            var mainFields = Object.keys(this);

            OriginalData = {};

            for (var i = 0; i < mainFields.length; i++) {
                var tmpField = mainFields[i];
                if(typeof(this[tmpField]) != "NixDataObjectReference_Array" && typeof(this[tmpField]) != "NixDataObjectReference_Object"){
                    OriginalData[tmpField] = this[tmpField];
                }
            }
            if(this.NixObjectState == 'loaded'){
                customCreated = false;
            }
            // OriginalData = angular.copy(this);
        };

        this.GetContextObject = function () {
            return {
                ID: this.ID,
                NixObjectID: this.NixObjectID,
                NixObjectState: this.NixObjectState,
                NixObjectType: this.NixObjectType
            };
        };

        this.GetNixDataObject = function () {
            return _nixDataObject;
        };

        this.isCustomCreated = function () {
            return customCreated;
        };

        this.NotifyDataContext = function () {
            if (_nixDataContext) {
                _nixDataContext.UpdateDataObject(this);
            }
        };

        this.SetOriginalData();
    };

    NixDataObject.prototype.ResetObject = function () {
        this.CheckHasExpired();

        var OriginalData = this.GetOriginalData();

        if (this.isCustomCreated() == false) {
            this.NixObjectState = Nix.Enum.NixDataObjectStates.Loaded;
            angular.extend(this, OriginalData);
        }
        else {
            this.NixObjectState = Nix.Enum.NixDataObjectStates.New;
            angular.extend(this, OriginalData);
        }


    };

    NixDataObject.prototype.IsDirty = function () {
        this.CheckHasExpired();
        var diffObj = this.getDifferences();
        if (Object.keys(diffObj).length > 4) {
            if (this.NixObjectState == Nix.Enum.NixDataObjectStates.Loaded) {
                this.NixObjectState = Nix.Enum.NixDataObjectStates.Updated;
            }

            return true;
        }

        return false;
    };

    NixDataObject.prototype.getDifferences = function () {
        this.CheckHasExpired();
        var diffObj = this.GetContextObject();

        var OriginalData = this.GetOriginalData();

        var keys = Object.keys(OriginalData);
        for (var i = 0; i < keys.length; i++) {
            if (keys[i] != '' && keys[i].toLowerCase().indexOf("nix") == -1) {
                if (OriginalData[keys[i]] != this[keys[i]] && typeof(OriginalData[keys[i]]) != 'object' && !(IsNullOrEmpty(this[keys[i]]) && IsNullOrEmpty(OriginalData[keys[i]]))) {
                    diffObj[keys[i]] = this[keys[i]];
                }
                else if (OriginalData[keys[i]] != this[keys[i]] && !(IsNullOrEmpty(this[keys[i]]) && IsNullOrEmpty(OriginalData[keys[i]]))) {
                  if (IsNullOrEmpty(this[keys[i]])){
                    diffObj[keys[i]] = this[keys[i]];
                    continue;
                  }
                    if (OriginalData[keys[i]] != null) {
                        if (OriginalData[keys[i]].getTime != undefined) {
                            if (OriginalData[keys[i]].getTime() != this[keys[i]].getTime())
                                diffObj[keys[i]] = this[keys[i]];
                        }
                        else if (JSON.stringify(OriginalData[keys[i]]) != JSON.stringify(this[keys[i]])) {
                            diffObj[keys[i]] = this[keys[i]];
                        }
                    }
                    else {
                        diffObj[keys[i]] = this[keys[i]];
                    }
                }
            }
        }

        return diffObj;
    };

    NixDataObject.prototype.GetObjectForDAL = function () {
        this.CheckHasExpired();

        var curState = this.GetObjectState();
        switch (curState) {
            case Nix.Enum.NixDataObjectStates.New:
                return this;
            case Nix.Enum.NixDataObjectStates.Updated:
                return this.getDifferences();
            case Nix.Enum.NixDataObjectStates.Deleted:
                // return this.GetContextObject();
                return this.getDifferences();
            default:
                return this;
        }
    };
//
    NixDataObject.prototype.NormalizeFields = function (objectFields) {
        this.CheckHasExpired();
        this.NormalizeData(this, objectFields);
    };

    NixDataObject.prototype.DateFormat = function (date) {
        if (date != null) {
            var d = new Date(date);
            var curr_date = d.getDate();
            var curr_month = d.getMonth() + 1;
            var curr_year = d.getFullYear();
            //var curr_hour = d.
            return curr_date + "/" + curr_month + "/" + curr_year;

        }
        else {
            return null;
        }

    };

    NixDataObject.prototype.DateSelectFormat = function (date) {
        return date.getDate();
    };

    NixDataObject.prototype.GetObjectState = function () {
        this.CheckHasExpired();

        if (this.NixObjectState == Nix.Enum.NixDataObjectStates.Loaded && this.IsDirty()) {
            return Nix.Enum.NixDataObjectStates.Updated;
        }

        return this.NixObjectState;
    };

    NixDataObject.prototype.DeleteObject = function () {
        this.CheckHasExpired();
        if (angular.isDefined(this.DeletedOn)) {
            this.DeletedOn = new Date();
            this.NixObjectState = Nix.Enum.NixDataObjectStates.Deleted;

        }
        else{
            // console.log("This object cant be deleted!");
            this.NixObjectState = Nix.Enum.NixDataObjectStates.Deleted;
        }
        // this.NixObjectState = Nix.Enum.NixDataObjectStates.Deleted;
    };

    NixDataObject.prototype.SyncRequired = function () {
        this.CheckHasExpired();

        var customCreated = this.isCustomCreated();

        if (customCreated && this.NixObjectState != Nix.Enum.NixDataObjectStates.Deleted) {
            return true;
        }
        else if (customCreated == false) {
            if (this.IsDirty() || this.NixObjectState != Nix.Enum.NixDataObjectStates.Loaded) {
                return true;
            }
        }

        return false;
    };

    NixDataObject.prototype.IsValid = function (Field) {
        //check if field is valid

        var nixDataObject = this.GetNixDataObject().Fields;
        if (nixDataObject[Field]) {
            if (nixDataObject[Field].nullable == false && (this[Field] == null || this[Field] == "" || this[Field] == 0)) {
                return false;
            }
        }

        return true;
    };

    NixDataObject.prototype.UpdateObjectState = function (submitResult) {
        if (submitResult.NixObjectState == Nix.Enum.NixDataObjectStates.Updated) {
            this.NixObjectState = Nix.Enum.NixDataObjectStates.Loaded;
        }
        else if (submitResult.NixObjectState == Nix.Enum.NixDataObjectStates.New) {
            angular.extend(this, submitResult.mergeObject);
            this.NixObjectState = Nix.Enum.NixDataObjectStates.Loaded;
        }
        else if (submitResult.NixObjectState == Nix.Enum.NixDataObjectStates.Deleted) {
            this.NixObjectExpired = true;
            // this.NixObjectState = Nix.Enum.NixDataObjectStates.Loaded;
        }

        this.NotifyDataContext();
        this.SetOriginalData();
    };

    NixDataObject.prototype.NormalizeData = function (data, objectFields) {
        var keys = Object.keys(objectFields);
        for (var i = 0; i < keys.length; i++) {
            var tmp = keys[i];
            if (tmp in data) {
                if (objectFields[tmp].datatype == 'int') {
                    if (typeof(objectFields[tmp].datatype) == 'string' && data[tmp] != null) {
                        data[tmp] = parseInt(data[tmp]);
                    }
                }
                else if (objectFields[tmp].datatype == 'datetime') {
                    if (objectFields[tmp] != null) {
                        if (data[tmp] != null) {
                            data[tmp] = new Date(data[tmp]);
                        }
                        else {
                            //data[tmp] = new Date();
                            //
                            //if (objectFields[tmp].name.toLowerCase().indexOf("due") > -1) {
                            //    data[tmp].setDate(data[tmp].getDate() + 14);
                            //}
                        }
                    }
                }
                else if (objectFields[tmp].datatype == 'decimal') {

                }
                else if (objectFields[tmp].datatype == Nix.AppEnum.DataTypes.RefArray) {
                    data[tmp] = new NixDataObjectReference_Array(objectFields[tmp], data[tmp], this, this.getObjectsDataContext());
                }
                else if (objectFields[tmp].datatype == Nix.AppEnum.DataTypes.RefObject) {
                    data[tmp] = new NixDataObjectReference_Object(objectFields[tmp], data[tmp], this, this.getObjectsDataContext());
                }
            }
        }
        return data;
    }

    return NixDataObject;
}]);
