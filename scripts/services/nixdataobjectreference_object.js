﻿"use strict";

NixDataModule.service("NixDataObjectReference_Object", ['$rootScope', function ($rootScope) {

    var NixDataObjectReference_Object = function (nixDataObject, data, parent, dataContext) {

        this.Init = false;
        this.DataContext = dataContext.GetDataContext(Nix.DAL[nixDataObject.model]);
        this.nixDataObject = nixDataObject;
        this.Data = [];
        this.parent = parent;

        if (data) {
            this.Init = true;
            this.Data = this.DataContext.setData([data]);
        }


    };

    NixDataObjectReference_Object.prototype.IsDirty = function () {
        if (this.Init == false) {
            throw "Reference object not initialized"
        }
        if (this.Data[0].isDirty()) return true;

        return false;
    };

    NixDataObjectReference_Object.prototype.Remove = function () {
        if (this.Init == false) {
            throw "Reference object not initialized"
        }
        ;
        this.parent[this.nixDataObject.field] = null;

    };

    NixDataObjectReference_Object.prototype.Delete = function () {
        if(this.Init == false){
            throw "Reference object not initialized"
        };
        this.Data[0].DeleteObject();
    };

    NixDataObjectReference_Object.prototype.SetData = function (item) {
        if(this.Init == false){
            throw "Reference object not initialized"
        };

        this.Data = this.DataContext.setData(item);
    };

    NixDataObjectReference_Object.prototype.GetData = function () {
        if(this.Init == false){
            throw "Reference object not initialized"
        };
        return this.Data[0];
    };

    return NixDataObjectReference_Object;
}]);
