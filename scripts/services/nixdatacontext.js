﻿"use strict";

if (Nix == undefined) {
    var Nix = {};
}

if (Nix.Enum == undefined) {
    Nix.Enum = {};
}

Nix.Enum.DataContextStates = {
    New: "new",
    Requested: "requested",
    Error: "error",
    Completed: "completed",
};

NixDataModule.service("NixDataContext", ['uuid2', 'NixDataObject', 'RestifyHelper', function (uuid2, NixDataObject, RestifyHelper) {

    var NixDataContext = function (nixDataObject) {
        var dataContextes = [];
        this.isMainDataContext = true;

        this.GetDataContext = function (NixDataObject) {
            if (this.NixObjectType == NixDataObject.NixObjectType)
                return this;

            for (var i = 0; i < dataContextes.length; i++) {
                var dc = dataContextes[i];
                if (dc.NixObjectType == NixDataObject.NixObjectType) {
                    return dc;
                }
            }

            var tmpDC = new NixDataContext(NixDataObject);
            tmpDC.isMainDataContext = false;

            dataContextes.push(tmpDC);
            return tmpDC;

        }.bind(this);

        this.ID = uuid2.newguid();
        this.NixObjectType = nixDataObject.NixObjectType;
        this.NixDataObject = nixDataObject;
        this.Filters = this.Filters = {
            AND: {},
            OR: {}
        };
        this.OrderBy = null;
        this.Paging = null;
        this.DataLoadOptions = [];
        this.Data = [];
        this.State = Nix.Enum.DataContextStates.New;
    };

    NixDataContext.prototype.PopulateDataLoadOptions = function () {
        this.AddDataLoadOptions(this.NixDataObject);
    };

    NixDataContext.prototype.AddDataLoadOptions = function (nixDataObject) {
        var obj = this.DataLoadOptions;
        if (obj == undefined) obj = [];
        this.DataLoadOptions = obj;
        // this.DataLoadOptions[nixDataObject.NixObjectType] = obj;

        var field = null;
        for (field in nixDataObject.Fields) {
            var tmpObj = field;
            if (nixDataObject.Fields[field].datatype != Nix.AppEnum.DataTypes.RefArray && nixDataObject.Fields[field].datatype != Nix.AppEnum.DataTypes.RefObject) {
                this.DataLoadOptions[nixDataObject.NixObjectType].push(tmpObj);
            }
            else if (nixDataObject.Fields[field].datatype == Nix.AppEnum.DataTypes.RefArray) {
                if (this.DataLoadOptions[nixDataObject.NixObjectType].indexOf(field) != -1) {
                    this.GetDataContext(Nix.DAL[nixDataObject.Fields[field].model]);
                }
            }
        }
        // for (var i = 0; i < nixDataObject.Fields.length; i++) {
        //     var tmpObj = nixDataObject.Fields[i];
        //     this.dataLoadOptions[nixDataObject.NixObjectType].push(tmpObj.name);
        // }

    };

    NixDataContext.prototype.setData = function (data) {
        var tmpNewDO = [];
        for (var i = 0; i < data.length; i++) {
            var tmpNDO = new NixDataObject(this.NixDataObject, this, data[i]);
            this.Data.push(tmpNDO);
            tmpNewDO.push(tmpNDO);
        }

        return tmpNewDO;
    };



    NixDataContext.prototype.setTotalItems = function (TotalItems) {
        if (this.Paging) {
            this.Paging.totalItems = TotalItems;
        }
    };

    NixDataContext.prototype.UpdateDataObject = function (data) {
        if (data.NixObjectExpired && this.Data.indexOf(data) >= 0) {
            this.Data.splice(this.Data.indexOf(data), 1);
        }
    };

    NixDataContext.prototype.createNew = function () {
        var newObj = new NixDataObject(this.NixDataObject, this);
        this.Data.push(newObj);
        return newObj;
    };

    NixDataContext.prototype.getRefDataContext = function () {
        return {
            ID: this.ID,
            NixObjectType: this.NixObjectType,
            Filters: this.Filters,
            Paging: this.Paging,
            DataLoadOptions: this.DataLoadOptions,
            OrderBy: this.OrderBy,
            Data: []
        };
    };

    NixDataContext.prototype.addFilter = function (name, value, operator) {
        if (this.isMainDataContext == false) {
            throw "Not a main data context";
        }

        if (angular.isDefined(name.name)) {
            name = name.name;
        }
        if (!angular.isDefined(operator)) {
            operator = "AND";
        }

        if (angular.isObject(value) && !angular.isArray(value)) {
            if (angular.isDefined(value.Operator)) {
                this.Filters[operator][name] = value;
            }
            else {
                if (this.NixDataObject.Fields[name].datatype == 'string' || this.NixDataObject.Fields[name].datatype == 'text') {
                    value.Operator = 'Like N';
                }
                else {
                    value.Operator = '=';
                }
                this.Filters[operator][name] = value;
            }
        }
        else {
            if (value == null) {
                this.Filters.AND[name] = {Value: 'NULL', Operator: 'is'};
            }
            else if (angular.isArray(value)) {
                this.Filters[operator][name] = {OR: []};
                for (var i = 0; i < value.length; i++) {
                    var obj = value[i];
                    if (angular.isDefined(obj.Operator)) {
                        this.Filters.AND[name].OR.push({Value: obj.Value, Operator: obj.Operator});
                    }
                    else {
                        if (this.NixDataObject.Fields[name].datatype == 'string' || this.NixDataObject.Fields[name].datatype == 'text') {
                            this.Filters.AND[name].OR.push({Value: obj, Operator: 'Like N'});
                        }
                        else {
                            this.Filters.AND[name].OR.push({Value: obj, Operator: '='});
                        }
                    }
                }
            }
            else {
                if (angular.isDefined(value.Operator)) {
                    this.Filters.AND[name] = {Value: value.Value, Operator: value.Operator};
                }
                else {
                    if (this.NixDataObject.Fields[name].datatype == 'string' || this.NixDataObject.Fields[name].datatype == 'text') {
                        this.Filters.AND[name] = {Value: value, Operator: 'Like N'};
                    }
                    else {
                        this.Filters.AND[name] = {Value: value, Operator: '='};
                    }
                }

            }
            //else {
            //    if (this.NixDataObject.Fields[name].datatype == 'string' || this.NixDataObject.Fields[name].datatype == 'text') {
            //        this.Filters.AND[name] = {Value: value, Operator: 'Like N'};
            //    }
            //    else{
            //        this.Filters.AND[name] = {Value: value, Operator: '='};
            //    }
            //}
        }
    };

    NixDataContext.prototype.removeFilter = function (name) {
        if (this.isMainDataContext == false) {
            throw "Not a main data context";
        }

        delete this.Filters[name];
    };

    NixDataContext.prototype.clearFilter = function () {
        if (this.isMainDataContext == false) {
            throw "Not a main data context";
        }

        this.Filters = {
            AND: {},
            OR: {}
        };
    };

    NixDataContext.prototype.SetOrderBy = function (field, direction) {
        var fieldName = "";

        if (typeof(field) == "object")
            fieldName = field.name;
        else {
            fieldName = field;
        }

        this.OrderBy = {
            field: fieldName,
            direction: direction
        };
    };

    NixDataContext.prototype.IsDirty = function () {
        for (var i = 0; i < this.Data.length; i++) {

            if (this.Data[i].NixObjectState == Nix.Enum.NixDataObjectStates.New) {
                return true;
            }
            else if (this.Data[i].GetOriginalData().NixObjectState == Nix.Enum.NixDataObjectStates.Loaded && this.Data[i].NixObjectState == Nix.Enum.NixDataObjectStates.Deleted) {
                return true;
            }
            else if (this.Data[i].IsDirty() && this.Data[i].NixObjectState != Nix.Enum.NixDataObjectStates.Deleted) {
                return true;
            }
        }
        return false;
    };

    NixDataContext.prototype.LoadData = function () {
        this.Data = [];
        return RestifyHelper.post_nixdata(this);
    };

    NixDataContext.prototype.setPage = function (page, size) {
        //this.Data.length = 0;
        this.Paging = {
            page: page,
            size: size
        }
    };

    NixDataContext.prototype.ResetAllObjects = function () {
        var index = 0;

        while (index < this.Data.length) {
            if (this.Data[index].isCustomCreated()) {
                this.Data.splice(index, 1);
            }
            else {
                this.Data[index].ResetObject();
                index++;
            }
        }
    };

    NixDataContext.prototype.GetModifiedNixObjects = function () {
        var updatedItems = [];

        for (var i = 0; i < this.Data.length; i++) {
            var tmpNixObj = this.Data[i];
            if (tmpNixObj.SyncRequired()) {
                updatedItems.push(tmpNixObj);
            }
        }

        return updatedItems;
    };

    return NixDataContext;
}]);