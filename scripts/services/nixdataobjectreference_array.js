﻿"use strict";

NixDataModule.service("NixDataObjectReference_Array", ['$rootScope', function ($rootScope) {

    var NixDataObjectReference_Array = function (nixDataObject, data, parent, dataContext) {
        this.Init = false;
        this.DataContext = dataContext.GetDataContext(Nix.DAL[nixDataObject.model]);
        this.nixDataObject = nixDataObject;
        this.Data = [];
        this.parent = parent;

        if (data) {
            this.Init = true;
            this.Data = this.DataContext.setData(data);
        }

    };

    NixDataObjectReference_Array.prototype.IsDirty = function () {
        if (this.Init == false) {
            throw "Reference array not initialized"
        }
        for (var i = 0; i < this.Data.length; i++) {
            var obj = this.Data[i];
            if (obj.isDirty()) return true;
        }
        return false;
        //check for a dirty obj and return true if found ONE or more
    };

    NixDataObjectReference_Array.prototype.Insert = function () {
        if (this.Init == false) {
            throw "Reference array not initialized"
        }
        //add item to datacontext and push in this.Data
        var tmpObj = this.DataContext.createNew();
        //connect reference fields
        tmpObj[this.nixDataObject.fk_field] = this.parent[this.nixDataObject.field];
        if (this.parent[this.nixDataObject.field] == null) {
            tmpObj.RefObjectID = this.parent.NixObjectID;
        }
        this.Data.push(tmpObj);

        return tmpObj;

    };

    NixDataObjectReference_Array.prototype.ResetAllObjects = function(){
        this.DataContext.ResetAllObjects();
    };

    NixDataObjectReference_Array.prototype.GetModifiedNixObjects = function(){
        var updatedItems = [];

        for (var i = 0; i < this.Data.length; i++) {
            var tmpNixObj = this.Data[i];
            if (tmpNixObj.SyncRequired()) {
                updatedItems.push(tmpNixObj);
            }
        }

        return updatedItems;
    };



    NixDataObjectReference_Array.prototype.Remove = function (item) {
        if (this.Init == false) {
            throw "Reference array not initialized"
        }
        var index = this.Data.indexOf(item);
        if (index != -1) {
            this.Data[index][this.nixDataObject.fk_field] = null;
            this.Data[index].RefObjectID = null;
        }
    };

    NixDataObjectReference_Array.prototype.Delete = function (item) {
        if (this.Init == false) {
            throw "Reference array not initialized"
        }
        var index = this.Data.indexOf(item);
        if (index != -1) {
            this.Data[index].DeleteObject();
        }

    };

    return NixDataObjectReference_Array;
}]);
