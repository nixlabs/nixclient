"use strict";

if (Nix == undefined) {
    var Nix = {};
}

if (Nix.Enum == undefined) {
    Nix.Enum = {};
}

Nix.Enum.NixDataObjectStates = {
    New: "new",
    Loaded: "loaded",
    Updated: "updated",
    Deleted: "deleted"
};

Nix.Enum.DataContextStates = {
    New: "new",
    Requested: "requested",
    Error: "error",
    Completed: "completed",
};

var module = module || {};
module.exports = Nix;