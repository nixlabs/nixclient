"use strict";

if (Nix == undefined) {
    var Nix = {};
}

if (Nix.AppEnum == undefined) {
    Nix.AppEnum = {};
}

Nix.AppEnum.OfferStates = {
    Draft: "draft",
    Sent: "sent",
    Canceled: "canceled"
};

Nix.AppEnum.InvoiceStates = {
    Draft: "draft",
    Sent: "sent",
    Payed: "payed",
    Expected: "expected",
    Canceled: "canceled"
};

Nix.AppEnum.ExpensesStates = {
    Draft: "draft",
    Expected: "expected",
    Received: "received",
    Payed: "payed",
    Canceled: "canceled"
};


Nix.AppEnum.TransactionTypes = {
    //Internal: "Income",
    //ExtToInter: "Expense",
    //InterToExt: "Internal"
    Internal: "Internal",
    ExtToInter: "Income",
    InterToExt: "Expense"
};
Nix.AppEnum.ContractPeriods = [{
    Monthly: "monthly"
},
    {
        Weekly: "weekly"
    }];

Nix.AppEnum.FinancePlanPeriods = [{
    Yearly: "yearly"
}
];

Nix.AppEnum.CompaniesSize = {
    4: "??????",
    3: "??????",
    2: "????",
    1: "?????"
};

Nix.AppEnum.DataTypes = {
    String: "String",
    RefObject: "reference_object",
    RefArray: "reference_array"
};
Nix.AppEnum.FileProcessTypes = {
    uploaded: "Uploaded",
    deleted: "Deleted",
    uploading: "Uploading"
};

var module = module || {};
module.exports = Nix;
